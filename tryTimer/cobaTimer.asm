.include "m8515def.inc"
.def counter = r20
.def counter1 = r21

.def temp = r16 ; temporary register
.def EW = r23 ; for PORTA
.def PB = r24 ; for PORTB
.def A  = r25

.org $00
	rjmp RESET
.org $07
	rjmp ISR_TOV0

RESET:
	ldi counter, 20
	ldi counter1, 2
	ldi	r16,low(RAMEND)
	out	SPL,r16	            ;init Stack Pointer		
	ldi	r16,high(RAMEND)
	out	SPH,r16

	ldi r16, (1<<CS02)|(1<<CS00)	; 
	out TCCR0,r16			
	ldi r16,1<<TOV0
	out TIFR,r16		; Interrupt if overflow occurs in T/C0
	ldi r16,1<<TOIE0
	out TIMSK,r16		; Enable Timer/Counter0 Overflow int
	ser r16
	out DDRB,r16		; Set port B as output
	sei

forever:
	rjmp forever

ISR_TOV0:
	push r16
	in r16,SREG
	push r16
	in r16,PORTB	; read Port B
	
	subi counter, 1

	;cpi counter, 4;
	;breq LED_4;
;
;	cpi counter, 5;
;	breq LED_5
;
;	cpi counter, 1;
;	breq LED_1;
;
;	cpi counter, 2;
;	breq LED_2;
;
;	cpi counter, 3;
;	breq LED_3;
;
	;cpi counter1, 1
	;breq LED_11

	cpi counter, 0
	breq LED_22
	

LED_1:
	ldi r16, 0b00000000			; invert bits of r16 
	out PORTB,r16	; write Port B
	pop r16
	out SREG,r16
	pop r16
	subi counter1, 1
	reti

LED_2:
	ldi r16, 0b00000010			; invert bits of r16 
	out PORTB,r16	; write Port B
	pop r16
	out SREG,r16
	pop r16
	reti

LED_3:
	ldi r16, 0b00000100		; invert bits of r16 
	out PORTB,r16	; write Port B
	pop r16
	out SREG,r16
	pop r16
	reti

LED_4:
	ldi r16, 0b00001000		; invert bits of r16 
	out PORTB,r16	; write Port B
	pop r16
	out SREG,r16
	pop r16
	reti

LED_5:
	ldi r16, 0b00010000		; invert bits of r16 
	out PORTB,r16	; write Port B
	pop r16
	out SREG,r16
	pop r16
	reti

LED_11:
	ldi r16, 0b11111111			; invert bits of r16 
	out PORTB,r16	; write Port B
	pop r16
	out SREG,r16
	pop r16
	ldi counter, 6
	reti

LED_22:
	ldi r16, 0b10101010	
	com r16		; invert bits of r16 
	out PORTB,r16	; write Port B
	pop r16
	out SREG,r16
	pop r16
	ret



